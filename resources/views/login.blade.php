<!DOCTYPE html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <title>Login to Ink And Paper</title>

    </head>
        <body>
            <h1>Login to Ink And Paper</h1>
            <form action="user" method="post" accept-charset="UTF-8">
                @csrf
                <div>
                  <label for="email">Email:</label>
                  <input type="text" name="email" id="email" />
                </div>
                <div>
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" />
                  </div>

                  <div>
                    <button type="submit">Login</button>
                  </div>

                </form>
            </body>

    </html>
